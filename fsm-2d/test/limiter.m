function phi = limiter(r,method)
% flux limiter
if nargin<1, 
    method = 1;
end
switch method
    case 2
        phi = (abs(r)+r)./(1+abs(r));                       % van Leer
    case 1
        phi = max(0,max(min(1,2*r),min(r,2)));               % Superbee
end
