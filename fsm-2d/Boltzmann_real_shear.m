clear; clc; tic


method=1;
Tl=2.5;   Tr=2;
ul=0;       ur=0;
rhol=1; rhor=0.125;

tau=0.2;           %%% Knudsen number
%%%%%%%%%
L=15;
Nx=100;     x=linspace(0,2,Nx);        delta_x=x(2)-x(1);
Nv=48;
Lx=L;	 Nvx=Nv;    vx=linspace(-Lx,Lx,Nvx);    delta_vx=vx(2)-vx(1);      fre_vx=(-Nvx/2:Nvx/2-1)*2*pi/(Nvx*2*Lx/(Nvx-1));
Ly=L;    Nvy=Nv;    vy=linspace(-Ly,Ly,Nvy);    delta_vy=vy(2)-vy(1);      fre_vy=(-Nvy/2:Nvy/2-1)*2*pi/(Nvy*2*Ly/(Nvy-1));

Delta=delta_vx*delta_vy;
%%
CFL=0.8;
delta_t=delta_x/max(abs(vx))*CFL; %%% time step
%%
[Fre_vx,Fre_vy]=ndgrid(fre_vx,fre_vy);

%% calculate kernel modes
R0=2*Ly/(3+sqrt(2)); 
R=R0*sqrt(2);    % remove the aliasing error

M=6;  % for phi      
integral_loop=M;
alphap=zeros(Nvx,Nvy,integral_loop);      
alphaq=alphap; 

alpha3=0;
for lp=1:M
    theta=lp*pi/M;
    s=Fre_vx*cos(theta+pi/2)+Fre_vy*sin(theta+pi/2);
    s(s==0)=1e-16;
    alphaq(:,:,lp)=2*R*sin(R*s)./(R*s)*(pi/M);
    s=Fre_vx*cos(theta)+Fre_vy*sin(theta);
    s(s==0)=1e-16;
    alphap(:,:,lp)=2*R*sin(R*s)./(R*s);
    
end
alphap=alphap/pi;  % the collision kernel B is 1/pi
alpha3=sum(alphap.*alphaq,3); % for the loss term

clear Fre_vx Fre_vy 

%%
%%% find out where the x-component velocity becomes positive
[value,index]=find(vx>0);    V_mid=index(1);
clear value index
[Vx,Vy]=meshgrid(vx,vy);
clear vx vy vz

V_dist_l=rhol/(pi*Tl)*exp(-((Vx-ul).^2+Vy.^2)/Tl);
V_dist_r=rhor/(pi*Tr)*exp(-((Vx-ur).^2+Vy.^2)/Tr);
x_step=x(1)/2+x(end)/2;


for loop=1:Nx
    if x(loop)<=x_step
        f(:,:,loop)=V_dist_l;
    else
        f(:,:,loop)=V_dist_r; 
    end
    A1(:,:,loop)=Vx;    A2(:,:,loop)=Vy;   
end
clear V_dist_l V_dist_r F x_step
% %%%%%%%%%%%%%%%%%%%%%%%%
temp2=zeros(size(f)); %% Q_{i}-Q_{i-1}
disc_vel=zeros(size(f));
V2=Vx.^2+Vy.^2;
%%%%%%%%%%%%%%%
for loop_time=1:0.15/delta_t
    loop_time    
    temp2(:,:,2:end)=f(:,:,2:end)-f(:,:,1:end-1);
    %%% for non-negative vx
    temp3=circshift(temp2(:,V_mid:end,:),[0,0,-1]); %%% Q_{i+1}-Q_{i}
    delta_1=limiter(temp2(:,V_mid:end,:)./(temp3+1e-15),method).*temp3;
    temp3=circshift(temp2(:,V_mid:end,:),[0,0,1]); %%% Q_{i-1}-Q_{i-2}     
    temp3(:,:,1)=0;  
    delta_2=limiter(temp3./(temp2(:,V_mid:end,:)+1e-15),method).*temp2(:,V_mid:end,:);     
    d_p=A1(:,V_mid:end,:).*      ( temp2(:,V_mid:end,:) ...
             +1/2*(1-A1(:,V_mid:end,:)*delta_t/delta_x) ...
               .*(delta_1-delta_2));
           %%% for negative vx      
    temp3=circshift(temp2(:,1:V_mid-1,:),[0,0,-1]);  %%% Q_{i+1}-Q_{i}
    temp1=circshift(temp3,[0,0,-1]); %%% Q_{i+2}-Q_{i+1}
    temp1(:,:,end)=0;
    delta_1=limiter(temp1./(temp3+1e-15),method).*temp3;
    delta_2=limiter(temp3./(temp2(:,1:V_mid-1,:)+1e-15),method).*temp2(:,1:V_mid-1,:);
    d_n=A1(:,1:V_mid-1,:).*             (temp3 ...
           -1/2*(1+A1(:,1:V_mid-1,:)*delta_t/delta_x) ...
              .*(delta_1-delta_2));
     %%% discrete of velocity
    disc_vel(:,1:V_mid-1,:)=d_n;
    disc_vel(:,V_mid:end,:)=d_p;
    temp=f-delta_t/delta_x*disc_vel;    
    
    %%%%%%%%%%%%%%%%  
    rho=reshape((sum(sum(temp)))*Delta,1,Nx);     
    ux=reshape((sum(sum(A1.*temp)))*Delta,1,Nx)./rho;     
    E=reshape((sum(sum((A1.^2+A2.^2).*temp)))*Delta,1,Nx);     
    %%%%%%%%%%%%%%%%%%%%%%
     p=E-rho.*ux.^2;
     T=p./rho;
epsilon=tau/0.5./rho;
     for loop=1:Nx  
        
        % collision
        f_spec=fftshift( ifft2( fftshift( f(:,:,loop) ) ) );

        f_tmp=0;
        for lp=1:integral_loop
            f_tmp=f_tmp+fft2(f_spec.*alphaq(:,:,lp)).*fft2(f_spec.*alphap(:,:,lp));
        end
        f_tmp=f_tmp-fft2( f_spec ).*fft2( f_spec.*alpha3 );
        f_coll=real( fftshift(f_tmp) );

        f(:,:,loop)=temp(:,:,loop)+delta_t/tau*( f_coll);
    

     end 

end
toc





