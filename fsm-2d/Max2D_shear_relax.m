
clear

delta_t=1/200;

%% uniform velocity and frequency space
Nv=64;
L=8*sqrt(2);
Lx=L;	 Nvx=Nv;    vx=linspace(-Lx,Lx,Nvx);    delta_vx=vx(2)-vx(1);      fre_vx=(-Nvx/2:Nvx/2-1)*2*pi/(Nvx*2*Lx/(Nvx-1));
Ly=L;    Nvy=Nv;    vy=linspace(-Ly,Ly,Nvy);    delta_vy=vy(2)-vy(1);      fre_vy=(-Nvy/2:Nvy/2-1)*2*pi/(Nvy*2*Ly/(Nvy-1));

dV=delta_vy*delta_vx;

[Fre_vx,Fre_vy]=ndgrid(fre_vx,fre_vy);
[Vx,Vy]=ndgrid(vx,vy);  % 

%% calculate kernel modes
R0=2*Ly/(3+sqrt(2)); 
R=R0*sqrt(3);    % remove the aliasing error

M=6;  % for phi      
integral_loop=M;
alphap=zeros(Nvx,Nvy,integral_loop);      
alphaq=alphap; 

alpha3=0;
for lp=1:M
    theta=lp*pi/M;
    s=Fre_vx*cos(theta+pi/2)+Fre_vy*sin(theta+pi/2);
    s(s==0)=1e-16;
    alphaq(:,:,lp)=2*R*sin(R*s)./(R*s)*(pi/M);
    s=Fre_vx*cos(theta)+Fre_vy*sin(theta);
    s(s==0)=1e-16;
    alphap(:,:,lp)=2*R*sin(R*s)./(R*s);
    
end
alphap=alphap/pi;  % the collision kernel B is 1/pi
alpha3=sum(alphap.*alphaq,3); % for the loss term

clear Fre_vx Fre_vy 

%% initial velocity distribution function and the boundary conditions
c2=Vx.^2+Vy.^2;
f=2*(1/pi)*exp( -((Vx).^2+Vy.^2) ).*Vx.^4;


%% main
rho=sum(sum( f.*dV ));
Ux=sum(sum( Vx.*f.*dV ))/rho;
Uy=sum(sum( Vy.*f.*dV ))/rho;
Pxx=2*sum(sum( (Vx-Ux).^2.*f.*dV ));
T=sum(sum( f.*c2.*dV ))/rho;


for iteration_step=1:3000

    f_spec=fftshift( ifft2( fftshift(f) ) );
    
    f_tmp=0;
    for lp=1:integral_loop
        f_tmp=f_tmp+fft2(f_spec.*alphaq(:,:,lp)).*fft2(f_spec.*alphap(:,:,lp));
    end
    f_tmp=f_tmp-fft2( f_spec ).*fft2( f_spec.*alpha3 );
    f_coll=real( fftshift(f_tmp) );
    
    f=f+delta_t*( f_coll);
   
    rho=[rho,sum(sum( f.*dV )) ];
    ux=sum(sum( Vx.*f.*dV ))/sum(sum( f.*dV ));
    Ux=[Ux,ux];
    Uy=[Uy,sum(sum( Vy.*f.*dV ))];
    T=[T,sum(sum( f.*c2.*dV ))/sum(sum( f.*dV )) ]; 
    
    Pxx=[Pxx,2*sum(sum( (Vx-ux).^2.*f.*dV ))];

end

time=(1:length(Pxx))*delta_t;
plot(time,Pxx-T(1)*rho(1),'g')
hold on
plot(time,(Pxx(1)-T(1)*rho(1))*exp(-0.5*rho(1)*time),'r')


